interface IActor{
    first_name: string;
    last_name: string;
    age : number;
    isMarried?: boolean;
}

interface IRating{
    stars: number;
    comments: string[];
    printRating(): any; 
}

class Actor implements IActor{

    first_name : string;
    last_name : string;
    age: number;

    constructor(first_name: string, last_name:string, age: number) {
        this.first_name = first_name;
        this.last_name = last_name; 
        this.age = age;
    }
}

class Rating implements IRating{
    stars: number;
    comments: string[];

    constructor(stars: number, comments: string[]){
            this.stars = stars;
            this.comments = comments;
    }
    
    printRating()
    {
        let rating = this.stars * 2 ;
        console.log(rating);   
    }
}

class Movie {
 
    actors:Actor[];
    constructor(actors:Actor[]){
        this.actors = actors;
    }
    
}

class Adventure extends Movie {
        
        isEpic: boolean;
    
        constructor(actors: Actor[], isEpic: boolean){ 
        
                super(actors);
                this.isEpic = isEpic;
        }

        displayActors(){
            
            console.log("Actors are :" + this.actors[0]);
        }
}


let movieONE = new Movie([{"first_name":"Paul","last_name":"patilas","age":32}]);
let movieTW0 = new Movie([{"first_name":"Paul","last_name":"patilas","age":32}]);
let movieTHREE = new Movie([{"first_name":"Paul","last_name":"patilas","age":32}]);

let advMovie = new Adventure([{"first_name":"Paul","last_name":"patilas","age":32}], true);

advMovie.displayActors();



