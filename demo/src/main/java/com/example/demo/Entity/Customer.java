package com.example.demo.Entity;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;

@Entity
@Table(name = "Customers")
public class Customer {


    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "SURNAME", nullable = false)
    private String email;

    public Customer(){}

    public Customer(int id, String name, String surname){
        this.id = id;
        this.name = name;
        this.email = surname;
    }

    public Customer(String name, String s) {
        this.name = name;
    }

    public void setEmail(String surname) {
        this.email = surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
