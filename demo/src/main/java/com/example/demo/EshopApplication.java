package com.example.demo;

import com.example.demo.Entity.Customer;
import com.example.demo.repository.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.stream.Stream;

@SpringBootApplication
public class EshopApplication {

	public static void main(String[] args) {

		SpringApplication.run(EshopApplication.class, args);
	}

	@Bean
	CommandLineRunner init(CustomerRepository customerRepository) {
		return args -> {
			Stream.of("John", "Julie", "Jennifer", "Helen", "Rachel").forEach(name -> {
				Customer cust = new Customer(name, name.toLowerCase() + "@domain.com");
				customerRepository.save(cust);
			});
			customerRepository.findAll().forEach(System.out::println);
		};
	}
}
