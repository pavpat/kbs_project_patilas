package com.example.demo.Controller;

import com.example.demo.Entity.Customer;
import com.example.demo.repository.CustomerRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public class customerController {

    private CustomerRepository customerRepository;
    @GetMapping("/customers")
    public List<Customer> getCust(){
        return (List<Customer>) customerRepository.findAll();
    }

    @PostMapping("/customers")
    void addCust(@RequestBody Customer customer){
        customerRepository.save(customer);
    }

}
