package com.example.demo.repository;

import com.example.demo.Entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long> {

    //select all elements
    @Query("SELECT customer FROM Customer ")
    List<Customer>getAll();

    @Override
    void deleteAll();
}
